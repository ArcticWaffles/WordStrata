﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WordStrata
{
    public static class Levels
    {
        public static List<Level> GameLevels { get; }

        private static readonly Level level1;
        private static readonly Level level2;
        private static readonly Level level3;

        private static bool[,,] tiles1;
        private static bool[,,] tiles2;
        private static bool[,,] tiles3;

        private static ImageSource image1;

        static Levels()
        {
            tiles1 = new bool[5, 5, 2]
            {
                { {true, true }, {true, true }, {true, true }, {true, false}, {false, false } },
                { {true, true }, {true, true }, {true, true }, {true, true}, {true, false } },
                { {true, true }, {true, true }, {true, true }, {true, true}, {true, true } },
                { {true, false }, {true, true }, {true, true }, {true, true}, {true, true } },
                { {false, false }, {true, false }, {true, true }, {true, true}, {true, true } },
            };

            tiles2 = new bool[4, 6, 2]
            {
                { {false, false }, {true, false }, {true, true}, {true, true }, {true, false }, {false, false } },
                { {true, false }, {true, true }, {true, true}, {true, true }, {true, true }, {true, false } },
                { {true, false }, {true, true }, {true, true }, {true, true}, {true, true }, {true, false } },
                { {false, false }, {true, false }, {true, true}, {true, true }, {true, false }, {false, false } },
            };

            tiles3 = new bool[4, 6, 3]
            {
                { {true, true, true }, {true, true, true }, {true, true, true}, {true, true, true }, {true, true, true }, {true, true, true } },
                { {true, true, true }, {true, true, false }, {true, true, false}, {true, true, false }, {true, true, false }, {true, true, true } },
                { {true, true, true }, {true, true, false }, {true, true, false }, {true, true, false}, {true, true, false }, {true, true, true } },
                { {true, true, true }, {true, true, true }, {true, true, true}, {true, true, true }, {true, true, true }, {true, true, true } },
            };

            //image1 = new ImageSource();
            image1 = new BitmapImage(new Uri("pack://application:,,,/Images/clouds.png"));

            level1 = new Level(tiles1, Colors.CadetBlue, Colors.LightSlateGray, image1);
            //level2 = new Level(tiles2, Colors.DarkSeaGreen, Colors.DarkOliveGreen);
            // level3 = new Level(tiles3, Colors.LightSteelBlue, Colors.SlateGray);

            GameLevels = new List<Level>
            {
                level1,
                level2,
                level3
            };
        }
    }
}