﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Controls;

namespace WordStrata
{
    public class Level
    {
        public Level(bool[,,] tileLocations, Color baseColor, Color holeColor, ImageSource backgroundImage)
        {
            TileLocations = tileLocations;
            BaseColor = baseColor;
            HoleColor = holeColor;
            BackgroundImage = backgroundImage;
        }

        public bool[,,] TileLocations { get; }

        public ImageSource BackgroundImage { get; }

        public Color BaseColor { get; }

        public Color HoleColor { get; }
    }
}