﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Core;
using System.Windows.Media;

namespace WordStrata
{
    internal class GridsquareColorConverter : IMultiValueConverter
    {
        // Makes "deeper" tiles lighter so user can see background image underneath. 
        // Temporary solution until some kind of 3D visual layering effect can be implemented.

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var level = values[0] as Level;
            var gridsquare = values[1] as Gridsquare;
            var numberOfLayers = level.TileLocations.GetLength(2);
            double opacityLevel = 0;

            if (gridsquare is Tile)
            {
                opacityLevel = (gridsquare.Coords.Z + 1.0) / numberOfLayers;
            }

            Brush gridsquareColor = new SolidColorBrush(level.BaseColor)
            {
                Opacity = opacityLevel
            };
            return gridsquareColor;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}