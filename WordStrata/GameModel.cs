﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Windows.Controls;
using System.Windows.Media;

namespace WordStrata
{
    public class GameModel : IGameModel
    {
        public GameModel(HashSet<string> dictionary)
        {
            Dictionary = dictionary;
            LevelNumber = 1;
        }

        public Board GameBoard { get; private set; }

        public HashSet<string> Dictionary { get; }

        public ImageSource BackgroundImage { get; private set; }

        public Level Level { get; private set; }

        private int levelNumber;
        public int LevelNumber
        {
            get
            {
                return levelNumber;
            }

            set
            {
                if (value == levelNumber) return;
                levelNumber = value;
                GetLevel(levelNumber);
            }
        }

        private void GetLevel(int levelNumber)
        {
            if (LevelNumber <= Levels.GameLevels.Count)
            {
                Level = Levels.GameLevels[LevelNumber - 1];
                GameBoard = BoardGenerator.generateShapedBoard(Level.TileLocations);
                BackgroundImage = Level.BackgroundImage;
            }
            // TODO: else they have played all the levels-- give stats, provide options for revisiting old levels, etc.
        }
    }
}

// TODO: Eventually use GameModel for player and level info.
// TODO: Should previous boards be kept so the player can try the exact same level again for a higher score?